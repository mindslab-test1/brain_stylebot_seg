import torch
import torch.nn.functional as F
from tqdm import tqdm

from .dice_loss import dice_coeff

def normPRED(d):
    ma = torch.max(d)
    mi = torch.min(d)

    dn = (d-mi)/(ma-mi)

    return dn


def eval_net(net, loader, device):
    """Evaluation without the densecrf with the dice coefficient"""
    net.eval()
    n_val = len(loader)  # the number of batch
    tot = 0

    with tqdm(total=n_val, desc='Validation round', unit='batch', leave=False) as pbar:
        for batch in loader:
            imgs, true_masks = batch['image'], batch['mask']
            imgs = imgs.to(device=device, dtype=torch.float32)
            mask_type = torch.float32
            true_masks = true_masks.to(device=device, dtype=mask_type)

            with torch.no_grad():
                d0, d1, d2, d3, d4, d5, d6 = net(imgs)
                mask_pred = d0

            pred = normPRED(mask_pred)
            tot += dice_coeff(pred, true_masks).item()
            pbar.update()

    return tot / n_val, imgs, true_masks, pred
