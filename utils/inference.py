import os
from skimage import io, transform
import cv2
import uuid
import torch
import torchvision
from torch.autograd import Variable
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils
# import torch.optim as optim

import numpy as np
from PIL import Image
import glob
import imdirect

from .data_loader import RescaleT
#from .data_loader import ToTensor
from .data_loader import ToTensorLab
from .data_loader import SalObjDataset, SalObjDataset_test
from .dice_loss import dice_coeff

from model import U2NET # full size version 173.6 MB
from model import U2NETP # small version u2net 4.7 MB

# normalize the predicted SOD probability map
def normPRED(d):
    ma = torch.max(d)
    mi = torch.min(d)

    dn = (d-mi)/(ma-mi)

    return dn

def save_output(image_name, pred, d_dir, out_threshold=.5):

    predict = pred.clone()
    predict = predict.squeeze()
    predict_org = predict.cpu().data.numpy()
    predict_np = np.ones(predict_org.shape)
    predict_np = predict_np*(predict_org > out_threshold)


    im = Image.fromarray(predict_np*255).convert('RGB')
    img_name = image_name.split("/")[-1]
    image = io.imread(image_name)
    imo = im.resize((image.shape[1], image.shape[0]), resample=Image.BILINEAR)

    pb_np = np.array(imo)

    aaa = img_name.split(".")
    bbb = aaa[0:-1]
    imidx = bbb[0]
    for i in range(1, len(bbb)):
        imidx = imidx + "." + bbb[i]
    namee = os.path.join(d_dir,imidx + '_mask.png')
    imo.save(namee)

    return namee


# def output(image_name, pred, d_dir):
#
#     predict = pred
#     predict = predict.squeeze()
#     predict_np = predict.cpu().data.numpy()
#
#     im = Image.fromarray(predict_np*255).convert('RGB')
#     img_name = image_name.split("/")[-1]
#     image = io.imread(image_name)
#     imo = im.resize((image.shape[1], image.shape[0]), resample=Image.BILINEAR)
#
#     pb_np = np.array(imo)
#
#     aaa = img_name.split(".")
#     bbb = aaa[0:-1]
#     imidx = bbb[0]
#     for i in range(1, len(bbb)):
#         imidx = imidx + "." + bbb[i]
#
#     imo = d_dir+imidx+'_mask.png'
#
#     return imo

def alphaimage(src, src_mask):
    img = Image.open(src)
    output = img.copy().convert('RGB')

    if img._getexif() is not None and img._getexif().get(274) is not None:
        img = imdirect.autorotate(img)
    height = img.size[1]
    width = img.size[0]
    if height > width:
        #result.shape[0] = 1024
        #result.shape[1] = (width * 1024/height)
        nheight = 1024
        nwidth = int(width * 1024/ height)
    else:
    #    if width<1024:
    #        nwidth = 1024
        nwidth = 1024
        #result.shape[1] = 1024
        #result.shape[0] = (height * 1024/width)
        nheight = int(height * 1024 / width)
    #    nwidth = 1024

    src_mask = src_mask.squeeze()
    src_mask = src_mask.cpu().data.numpy()
    mask = Image.fromarray(src_mask*255)
    mask = mask.convert('L')

    output = output.resize((nwidth, nheight))
    mask = mask.resize((nwidth, nheight))
    #assert output.size[0]==mask.size[1] and output.size[1]==mask.size[1]

    output.putalpha(mask)

    return output


# global object
model_name = 'u2net'  # u2netp
model_dir = os.path.join('saved_models',model_name, model_name + '.pth')
prediction_dir = 'tmp'
net = U2NET(3,1)
net.load_state_dict(torch.load(model_dir))
if torch.cuda.is_available():
    net.cuda()
net.eval()

def ApplyU2Net(infile):
    img_name_list = []
    img_name_list.append(infile)
    print(img_name_list)

    # --------- 2. dataloader ---------
    #1. dataloader
    test_salobj_dataset = SalObjDataset_test(img_name_list = img_name_list,
        transform=transforms.Compose([RescaleT(320),
        ToTensorLab(flag=0)])
        )
    test_salobj_dataloader = DataLoader(test_salobj_dataset,
        batch_size=1,
        shuffle=False,
        num_workers=1)

    # --------- 4. inference for each image ---------
    for i_test, data_test in enumerate(test_salobj_dataloader):

        print('inferencing:',img_name_list[i_test].split("/")[-1])

        inputs_test = data_test['image']
        inputs_test = inputs_test.type(torch.FloatTensor)

        if torch.cuda.is_available():
            inputs_test = Variable(inputs_test.cuda())
        else:
            inputs_test = Variable(inputs_test)

        d1,d2,d3,d4,d5,d6,d7= net(inputs_test)

        # normalization
        pred = d1[:,0,:,:]
        pred = normPRED(pred)

        # save results to test_results folder
        msk = save_output(img_name_list[i_test],pred,prediction_dir)

        #msk = output(img_name_list[i_test],pred,prediction_dir)

        del d1,d2,d3,d4,d5,d6,d7

    # --------- 5. create transparent image ---------

    #alphaimage(Image.fromarray(data_test['image'].numpy()), msk).save('result.png')
    #src = Image.fromarray(data_test['image'].numpy())
    #imgarray = data_test['image'][0]
    #imgarray_numpy = imgarray.numpy()
    #imgarray_numpy.transpose(2, 1, 0)

    src = img_name_list[0]
    outfname = os.path.join('tmp',src.split('/')[-1].split('.')[0] + '_result.png')
    #alphaimage(src, msk).save(outfname)
    result = alphaimage(src, pred)
    result.save(outfname)
    with open(outfname, 'rb') as f:
        bin = f.read()
        f.close()
    return bin


def U2Net_eval(imgdir, mskdir, outdir):
    model_name = 'u2net'  # u2netp
    model_dir = os.path.join('saved_models', model_name, model_name + '.pth')
    prediction_dir = 'tmp'
    net = U2NET(3, 1)
    net.load_state_dict(torch.load(model_dir))
    if torch.cuda.is_available():
        net.cuda()
    net.eval()
    img_name_list = sorted([x for x in os.listdir(imgdir) if x.split('.')[-1].lower() in ['png', 'jpeg', 'jpg']])
    mask_name_list = sorted([y for y in os.listdir(mskdir) if y.split('.')[-1].lower() in ['png', 'jpeg', 'jpg']])


    # --------- 2. dataloader ---------
    #1. dataloader
    test_salobj_dataset = SalObjDataset(img_name_list = img_name_list, label_name_list=mask_name_list,
        transform=transforms.Compose([RescaleT(320),
        ToTensorLab(flag=0)])
        )
    test_salobj_dataloader = DataLoader(test_salobj_dataset,
        batch_size=1,
        shuffle=False,
        num_workers=1)

    # --------- 4. inference for each image ---------

    for i_test, data_test in enumerate(test_salobj_dataloader):

        print('inferencing:',img_name_list[i_test].split("/")[-1])

        inputs_test = data_test['image']
        inputs_test = inputs_test.type(torch.FloatTensor)
        label = data_test['label']

        if torch.cuda.is_available():
            inputs_test = Variable(inputs_test.cuda())
        else:
            inputs_test = Variable(inputs_test)

        d1,d2,d3,d4,d5,d6,d7= net(inputs_test)

        # normalization
        pred = d1[:,0,:,:]
        pred = normPRED(pred)

        label.detach()
        pred.detach()

        output = cv2.hconcat([np.array(label), np.array(pred)])

        #msk = output(img_name_list[i_test],pred,prediction_dir)

        del d1,d2,d3,d4,d5,d6,d7



##################################################################################3



def main():
    # --------- 1. get image path and name ---------
    model_name='u2net'#u2netp

    image_dir = './test_data/test_images/'
    prediction_dir = './test_data/' + model_name + '_results/'
    model_dir = './saved_models/'+ model_name + '/' + model_name + '.pth'

    img_name_list = glob.glob(image_dir + '*')
    print(img_name_list)

    # --------- 2. dataloader ---------
    #1. dataloader
    test_salobj_dataset = SalObjDataset_test(img_name_list = img_name_list,
                                        transform=transforms.Compose([RescaleT(320),
                                                                      ToTensorLab(flag=0)])
                                        )
    test_salobj_dataloader = DataLoader(test_salobj_dataset,
                                        batch_size=1,
                                        shuffle=False,
                                        num_workers=1)

    # --------- 3. model define ---------
    if(model_name=='u2net'):
        print("...load U2NET---173.6 MB")
        net = U2NET(3,1)
    elif(model_name=='u2netp'):
        print("...load U2NEP---4.7 MB")
        net = U2NETP(3,1)
    net.load_state_dict(torch.load(model_dir))
    if torch.cuda.is_available():
        net.cuda()
    net.eval()

    # --------- 4. inference for each image ---------
    for i_test, data_test in enumerate(test_salobj_dataloader):

        print("inferencing:",img_name_list[i_test].split("/")[-1])

        inputs_test = data_test['image']
        inputs_test = inputs_test.type(torch.FloatTensor)

        if torch.cuda.is_available():
            inputs_test = Variable(inputs_test.cuda())
        else:
            inputs_test = Variable(inputs_test)

        d1,d2,d3,d4,d5,d6,d7= net(inputs_test)

        # normalization
        pred = d1[:,0,:,:]
        pred = normPRED(pred)

        # save results to test_results folder
        #save_output(img_name_list[i_test],pred,prediction_dir)

        msk = output(img_name_list[i_test],pred,prediction_dir)

        del d1,d2,d3,d4,d5,d6,d7

    # --------- 5. create transparent image ---------

    #alphaimage(Image.fromarray(data_test['image'].numpy()), msk).save('result.png')
    #src = Image.fromarray(data_test['image'].numpy())
    #imgarray = data_test['image'][0]
    #imgarray_numpy = imgarray.numpy()
    #imgarray_numpy.transpose(2, 1, 0)

    src = img_name_list[0]
    alphaimage(src, msk).save('result.png')

if __name__ == "__main__":
    main()
