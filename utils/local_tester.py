import os
from data_loader import SalObjDatasett
from data_loader import ToTensorLab
from data_loader import RescaleT
import torch
from torchvision import transforms
from torch.utils.data import Dataset, DataLoader
from torch.autograd import Variable


from u2net import U2NET # full size version 173.6 MB


def normPRED(d):
    ma = torch.max(d)
    mi = torch.min(d)

    dn = (d-mi)/(ma-mi)

    return dn

def U2Net_eval(imgdir, mskdir, outdir):
    model_name = 'u2net'  # u2netp
    model_dir = os.path.join('.', 'best_accuracy.pth')
    net = U2NET(3, 1)
    net.load_state_dict(torch.load(model_dir))
    if torch.cuda.is_available():
        net.cuda()
    net.eval()

    img_name_list = sorted([x for x in os.listdir(imgdir) if x.split('.')[-1].lower() in ['png', 'jpeg', 'jpg']])
    mask_name_list = sorted([y for y in os.listdir(mskdir) if y.split('.')[-1].lower() in ['png', 'jpeg', 'jpg']])


    # --------- 2. dataloader ---------
    #1. dataloader
    test_salobj_dataset = SalObjDatasett(img_name_list = img_name_list, lbl_name_list=mask_name_list,
                                         res_dir='.',
        transform=transforms.Compose([RescaleT(320),
        ToTensorLab(flag=0)])
        )
    test_salobj_dataloader = DataLoader(test_salobj_dataset,
        batch_size=1,
        shuffle=False,
        num_workers=1)

    # --------- 4. inference for each image ---------

    for i_test, data_test in enumerate(test_salobj_dataloader):

        print('inferencing:',img_name_list[i_test].split("/")[-1])

        inputs_test = data_test['image']
        inputs_test = inputs_test.type(torch.FloatTensor)
        label = data_test['label']

        if torch.cuda.is_available():
            inputs_test = Variable(inputs_test.cuda())
        else:
            inputs_test = Variable(inputs_test)

        d1,d2,d3,d4,d5,d6,d7= net(inputs_test)

        # normalization
        pred = d1[:,0,:,:]
        pred = normPRED(pred)

        label.detach()
        pred.detach()

        output = cv2.hconcat([np.array(label), np.array(pred)])
        cv2.imwrite(os.path.join(outdir, data_test['imname']), output)

        #msk = output(img_name_list[i_test],pred,prediction_dir)

        del d1,d2,d3,d4,d5,d6,d7

if __name__=='__main__':
    input_dir = './seg_img'
    mask_dir = './seg_mask'
    output_dir = './seg_output'
    U2Net_eval(input_dir, mask_dir, output_dir)