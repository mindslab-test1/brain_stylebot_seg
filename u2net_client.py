import sys
sys.path.append('..')
import grpc
from u2net_pb2 import PngFileBinary
from u2net_pb2_grpc import U2NetStub
import argparse
import os


CHUNK_SIZE = 1024 * 1024  # 1MB


def read_bytes(file_, num_bytes):
    while True:
        bin = file_.read(num_bytes)
        if len(bin) == 0:
            break
        yield bin


def read_test_file(img_dir, chunk_size):
    with open(img_dir, 'rb') as f:
        for byte in read_bytes(f, chunk_size*chunk_size):
            yield PngFileBinary(bin=byte)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Segmentationclient')
    parser.add_argument('-r', '--remote',
                        nargs='?',
                        dest='remote',
                        help='grpc: ip:port',
                        type=str,
                        default='0.0.0.0:5002')
    parser.add_argument('-i', '--input',
                        nargs='?',
                        dest='input',
                        help='image path',
                        type=str,
                        default='test_data/samples.png')
    parser.add_argument('-o', '--output',
                        nargs='?',
                        dest='output',
                        help='image output path',
                        type=str,
                        default= 'test_out')
    parser.add_argument('-cs', '--chunk-size',
                        nargs='?',
                        dest='chunk',
                        help='Chunk size for streaming input',
                        type=int,
                        default = 4096)

    args = parser.parse_args()

    with grpc.insecure_channel(args.remote, options=[('grpc.max_send_message_length', 20000000)]) as channel:
        stub = U2NetStub(channel)
        if not os.path.isdir(args.input):
            print(args.input)
            outputs = stub.StreamU2Net(read_test_file(args.input, args.chunk))
            png = bytearray()
            for png_binary in outputs:
                png.extend(png_binary.bin)
            filename = args.input.split('/')[-1].split('.')[0] + '.png'
            out_dir = os.path.join(args.output, filename)
            with open(out_dir, 'wb') as f:
                f.write(png)
                f.close()
        else:
            dir_list = [x for x in os.listdir(args.input)]
            if not os.path.isdir(os.path.join(args.output, args.input.split('/')[-1])):
                os.mkdir(os.path.join(args.output, args.input.split('/')[-1]))

            for ii, dire in enumerate(dir_list):
                name_list = [x for x in os.listdir(os.path.join(args.input, dire)) if x.split('.')[-1] in ['jpg', 'jpeg', 'png']]
                for index, name in enumerate(name_list):
                    print(name)
                    outputs = stub.StreamU2Net(read_test_file(os.path.join(args.input,dire, name), args.chunk))
                    png = bytearray()
                    for png_binary in outputs:
                        png.extend(png_binary.bin)
                    out_dir = os.path.join(args.output, args.input.split('/')[-1], dire)
                    if not os.path.isdir(out_dir):
                        os.mkdir(out_dir)
                    with open(os.path.join(out_dir, name.split(".")[0]+'.png'), 'wb') as f:
                        f.write(png)
                        f.close()
