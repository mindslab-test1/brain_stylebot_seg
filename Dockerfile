#FROM docker.maum.ai:443/jun3518:base
FROM nvcr.io/nvidia/cuda:10.1-cudnn7-runtime-ubuntu18.04

WORKDIR /app

COPY ./requirements_docker.txt .
#RUN python -m grpc_tools.protoc -I ./proto --python_out=. --grpc_python_out=. u2net.proto
RUN set -xe \
 && apt-get -y update \
 && apt-get install -y python-pip python3-dev
RUN pip install --upgrade pip
RUN apt-get install -y vim libsm6 libxext6 libxrender-dev
EXPOSE 5002

RUN apt-get install -y python3-venv
RUN python3 -m venv venv_u2net
RUN venv_u2net/bin/pip3 install --upgrade pip
RUN venv_u2net/bin/pip3 install -r requirements_docker.txt
RUN venv_u2net/bin/pip3 install protobuf --upgrade

COPY . ./stylebot-seg
WORKDIR stylebot-seg
RUN mkdir ./tmp
#RUN sh ./load_checkpoint.sh

ENTRYPOINT ../venv_u2net/bin/python3 u2net_server.py
#ENTRYPOINT python forever.py
