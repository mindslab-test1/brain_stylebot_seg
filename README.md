# Stylebot Segmentation API

## Usage

### Setup
```bash
python -m grpc.tools.protoc --proto_path=. --python_out=. --grpc_python_out=. ./proto/u2net.proto
```

### Run Server
Device는 default 0번
```bash
python u2net_server.py [OPTION] -d [device number]
```

### Run Client
input path default: ./test_data/samples.png

output path default: ./test_output

default chunk_size = 1MB
```bash
python u2net_client.py -r [Servicehost:port] -i [input path] -o [output path] -cs [chunk_size]
```

