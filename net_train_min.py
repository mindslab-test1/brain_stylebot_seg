import argparse
import os
import sys
import torch
import time
import datetime

import torch.nn as nn
from torch import optim
from tqdm import tqdm
from utils.eval import eval_net
import logging
import numpy as np

import torchvision.utils as vutils
from omegaconf import OmegaConf
from torch.utils.tensorboard import SummaryWriter
from utils.data_loader import NormDataset
from torch.utils.data import DataLoader
#from dataset import *

from model import U2NET

from torch.autograd import Variable


def muti_bce_loss_fusion(d0, d1, d2, d3, d4, d5, d6, labels_v, bce_loss):
    loss0 = bce_loss(d0,labels_v)
    loss1 = bce_loss(d1,labels_v)
    loss2 = bce_loss(d2,labels_v)
    loss3 = bce_loss(d3,labels_v)
    loss4 = bce_loss(d4,labels_v)
    loss5 = bce_loss(d5,labels_v)
    loss6 = bce_loss(d6,labels_v)
    loss = loss0 + loss1 + loss2 + loss3 + loss4 + loss5 + loss6
    return loss
	# print("l0: %3f, l1: %3f, l2: %3f, l3: %3f, l4: %3f, l5: %3f, l6: %3f\n"%(loss0.data[0],loss1.data[0],loss2.data[0],loss3.data[0],loss4.data[0],loss5.data[0],loss6.data[0]))


def train_net(network, device, args):
    net.cuda()

    dataset_tr = NormDataset(imgs_dir=args.train_img, mask_dir=args.train_mask, siz=args.size)
    dataset_vl = NormDataset(imgs_dir=args.val_img, mask_dir=args.val_mask, siz=args.size)

    train_loader = DataLoader(dataset_tr, batch_size=args.batch_size, shuffle=True, num_workers=args.num_worker, pin_memory=True)
    val_loader = DataLoader(dataset_vl, batch_size=int(args.batch_size*args.val_ratio), shuffle=False, num_workers=args.num_worker, pin_memory=True, drop_last=True)

    writer = SummaryWriter(log_dir=os.path.join(args.logdir, args.name),
                           comment= args.name)

    global_step = 0

    optimizer = optim.Adam(net.parameters(), lr=args.lr, betas=(0.9, 0.999), eps=1e-08, weight_decay=0)
    if args.scheduler:
        print("scheduler used.")
        scheduler = optim.lr_scheduler.ReduceLROnPlateau(optimizer, factor=args.scheduler_factor, patience=args.scheduler_pat)

    best_accuracy = -1

    for epoch in range(args.epochs):
        net.train()

        epoch_loss = 0
        with tqdm(total=len(dataset_tr), desc='epoch = ' + str(epoch + 1)+'/'+str(args.epochs), unit='img') as pbar:
            for batch in train_loader:
                imgs = batch['image']
                true_masks = batch['mask']

                imgs = imgs.to(device=device, dtype=torch.float32)
                mask_type = torch.float32
                true_masks = true_masks.to(device=device, dtype=mask_type)

                # wrap them in Variable
                if torch.cuda.is_available():
                    inputs_v, labels_v = Variable(imgs, requires_grad=False), Variable(true_masks, requires_grad=False)
                else:
                    inputs_v, labels_v = Variable(imgs, requires_grad=False), Variable(true_masks, requires_grad=False)


                # forward + backward + optimize
                d0, d1, d2, d3, d4, d5, d6 = net(inputs_v)
                criterion = nn.BCEWithLogitsLoss(size_average=True, reduction='mean')
                loss = muti_bce_loss_fusion(d0, d1, d2, d3, d4, d5, d6, labels_v, criterion)


                epoch_loss += loss.item()
                writer.add_scalar('Loss/train', loss.item(), global_step)

                pbar.set_postfix(**{'loss (batch)': loss.item()})

                optimizer.zero_grad()
                loss.backward()
                nn.utils.clip_grad_value_(net.parameters(), 0.1)
                optimizer.step()

                pbar.update(imgs.shape[0])
                global_step += 1

                writer.add_images('images_tr', imgs, global_step)
                writer.add_images('masks/true_tr', true_masks, global_step)
                writer.add_images('masks/pred_tr', d0, global_step)

                if global_step % (args.eval_start) == 0:
                    for tag, value in net.named_parameters():
                        tag = tag.replace('.', '/')
                        writer.add_histogram('weights/' + tag, value.data.cpu().numpy(), global_step)
                        writer.add_histogram('grads/' + tag, value.grad.data.cpu().numpy(), global_step)

                    val_score, val_img, val_mask, val_pred = eval_net(net, val_loader, device)
                    if args.scheduler:
                        scheduler.step(val_score)
                    writer.add_scalar('learning_rate', optimizer.param_groups[0]['lr'], global_step)

                    logging.info('Validation Dice Coeff: {}'.format(val_score))
                    writer.add_scalar('Dice/test', val_score, global_step)

                    writer.add_images('images', val_img, global_step)
                    writer.add_images('masks/true', val_mask, global_step)
                    writer.add_images('masks/pred', val_pred, global_step)
                    current_accuracy = val_score
                    if current_accuracy > best_accuracy:
                        if not os.path.isdir(args.dir_checkpoint):
                            os.mkdir(args.dir_checkpoint)
                        logging.info('Created checkpoint directory')

                        if not os.path.isdir(os.path.join(args.dir_checkpoint, args.name)):
                            os.mkdir(os.path.join(args.dir_checkpoint, args.name))

                        torch.save(net.state_dict(),
                                   os.path.join(args.dir_checkpoint, args.name, 'best_accuracy.pth'))
                                                
                        with open(os.path.join(args.dir_checkpoint, args.name, 'checkpoint_info.txt')) as f:
                            f.write(str(epoch+1) + "\t" + str(current_accuracy*100) + "%")


                        logging.info('Best accuracy model ' + str(epoch + 1) + ' is saved ! score:' + str(current_accuracy))



    writer.close()


def get_args():
    parser = argparse.ArgumentParser(description='Train the UNet on images and target masks',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-yaml', help=' yaml file directory')
    args = parser.parse_args()
    opt = OmegaConf.load(args.yaml)
    return opt


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format='%(levelname)s: %(message)s')
    args = get_args()
    device = torch.device(f'cuda:{args.gpu}' if torch.cuda.is_available() else 'cpu')
    logging.info('Using device '+str(device))

    args.train_img = os.path.join(args.train_dir, 'image')
    args.train_mask = os.path.join(args.train_dir, 'mask')

    args.val_img = os.path.join(args.valid_dir, 'image')
    args.val_mask = os.path.join(args.valid_dir, 'mask')

    args.logdir = os.path.join(args.logdir, 'runs')

    net = U2NET(3, 1)

    if args.name == "":
        args.name = 'LR_'+str(args.lr)+'_BS_'+str(args.batch_size)
    now = datetime.datetime.now()
    args.name += now.strftime('%Y-%m-%d %H:%M:%S')
    print(args.name)


    if os.path.isfile(args.load_model):
        net.load_state_dict(torch.load(args.load_model, map_location=device))
        print('load')
    else:
        print('new')

    net.to(device=device)
    # faster convolutions, but more memory
    # cudnn.benchmark = True

    try:
        train_net(network=net, device = device, args=args)

    except KeyboardInterrupt:
        torch.save(net.state_dict(), 'INTERRUPTED.pth')
        logging.info('Saved interrupt')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
