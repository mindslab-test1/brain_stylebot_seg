from concurrent import futures
import os
import uuid
# GRPC
import grpc
from u2net_pb2 import PngFileBinary
import u2net_pb2_grpc
# ENGIN
import cv2
from utils.inference import ApplyU2Net

CHUNK_SIZE = 1024 * 1024  # 1MB

def read_bytes(file_, num_bytes):
    while True:
        bin = file_.read(num_bytes)
        if len(bin) == 0:
            break
        yield bin


class U2NetServicer(u2net_pb2_grpc.U2NetServicer):
    def __init__(self, args):
        pass

    def StreamU2Net(self, request_iterator, context):
        print('Start StreamUNet')

        png = bytearray()
        for png_binary in request_iterator:
            png.extend(png_binary.bin)
        ### RECEIVE DONE

        ## SAVE AS PNG
        filename = str(uuid.uuid4()) + '.png'
        tmpfn = os.path.join('tmp',filename)
        with open(tmpfn, 'wb') as f:
            f.write(png)
        ## PNG SAVE DONE,


        byte = ApplyU2Net(tmpfn)

        for byte in read_bytes(f, CHUNK_SIZE):
            yield PngFileBinary(bin=byte)

def serve(args):
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    u2net_pb2_grpc.add_U2NetServicer_to_server(U2NetServicer(args), server)
    print('Starting server. Listening on port 5002.')
    server.add_insecure_port('[::]:5002')
    server.start()
    server.wait_for_termination()


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--device', type=int, default = 0, help ="device")
    args = parser.parse_args()
    serve(args)
