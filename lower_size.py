from PIL import Image
import os

def check_size(img_dir):
    dsize = os.path.getsize(img_dir)
    if dsize > 4194304:
        print(dsize)
        return False
    else:
        return True


if __name__ == '__main__':
    root = '/home/yangseungmin/Desktop/tmp/1117_실데이터 segmentation 요청'
    sub_fold = [x for x in os.listdir(root)]
    for sub in sub_fold:
        subdir = os.path.join(root, sub)
        image_list = [x for x in os.listdir(subdir)]
        for image in image_list:
            image_dir = os.path.join(subdir, image)
            if not check_size(image_dir):
                img = Image.open(image_dir)
                img = img.resize((1024,1024))
                img.save(image_dir)
                print(f"{image} is resized.")



